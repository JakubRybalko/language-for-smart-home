from enum import Enum, auto

class token_type(Enum):
    PARENTH_OPEN = auto()
    PARENTH_CLOSE = auto()
    BRACKET_OPEN = auto()
    BRACKET_CLOSE = auto()
    DOT = auto()
    COMA = auto()
    SEMICOLON = auto()
    IF = auto()
    WHILE = auto()
    ELSE = auto()
    FOREACH = auto()
    NEGATION = auto()
    ASSIGNMENT = auto()
    FUNCTION = auto()
    OR = auto()
    AND = auto()
    COMP_OPERATOR = auto()
    EQUALITY_OPERATOR = auto()
    ADD_OPERATOR = auto()
    MUL_OPERATOR = auto()
    ON = auto()
    EACH = auto()
    TIME_DESCRIBER = auto()
    QUOTE = auto()
    IDENTIFIER = auto()
    NUMBER_LITERAL = auto()
    INVALID = auto()
    UNDEFINED = auto()
    END_OF_FILE = auto()

class Location:
    line: int
    char_number: int

    def __init__(self, line, char_number):
        self.line = line
        self.char_number = char_number


class Token:
    def __init__(self, token_type, value = "", location = None):
        self.type = token_type
        self.value = value
        self.location = location
