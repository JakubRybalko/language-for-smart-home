from get_input import string_buffer
from Lexer import Lexer
from Parser import Parser
from interpreter import Interpreter


def app():
    file = open("files/test_file.txt", "r")
    s = string_buffer(file.read())
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    i.execute()

if __name__ == "__main__":
    app()