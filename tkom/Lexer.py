import string
from get_input import get_input
from Token import Token, token_type, Location
from Exceptions import QuoteError

class Lexer:

    def __init__(self, source: get_input):
        self.current_char = None
        self.input = source
        self.next_char = self.input.get_char()

        self._simple_tokens = {
            "+" : token_type.ADD_OPERATOR,
            "-" : token_type.ADD_OPERATOR,
            ";" : token_type.SEMICOLON,
            "(" : token_type.BRACKET_OPEN,
            ")" : token_type.BRACKET_CLOSE,
            "{" : token_type.PARENTH_OPEN,
            "}" : token_type.PARENTH_CLOSE,
            "*" : token_type.MUL_OPERATOR,
            "/" : token_type.MUL_OPERATOR,
            "." : token_type.DOT,
            "," : token_type.COMA,
            ";" : token_type.SEMICOLON,
        }

        self._complex_tokens = {
            "=" : token_type.ASSIGNMENT,
            "<" : token_type.COMP_OPERATOR,
            ">" : token_type.COMP_OPERATOR,
            "!" : token_type.NEGATION,
            "==" : token_type.EQUALITY_OPERATOR,
            "!=" : token_type.EQUALITY_OPERATOR,
            "<=" : token_type.COMP_OPERATOR,
            ">=" : token_type.COMP_OPERATOR,
            "||" : token_type.OR,
            "&&" : token_type.AND,
        }

        self._identifiers = {
            "foreach" : token_type.FOREACH,
            "while" : token_type.WHILE,
            "else" : token_type.ELSE,
            "each" : token_type.EACH,
            "fun" : token_type.FUNCTION,
            "on" : token_type.ON,
            "if" : token_type.IF,
            "ms" : token_type.TIME_DESCRIBER,
            "s" : token_type.TIME_DESCRIBER,
            "m" : token_type.TIME_DESCRIBER,
            "h" : token_type.TIME_DESCRIBER,
            "d" : token_type.TIME_DESCRIBER,
        }

    def get_next_char(self):
        self.current_char = self.next_char
        try:
            self.next_char = self.input.get_char()
        except StopIteration:
            self.next_char = None

    def next_token(self):
        self.get_next_char()
        while(self.current_char and (self.current_char in string.whitespace)):
            self.get_next_char()

        if(self.current_char is None):
            token = token_type.END_OF_FILE
            return Token(token)
        

        if(self.current_char.isalpha() or self.current_char == "_"):
            bufferList = []
            bufferList.append(self.current_char)
            while(self.next_char and (self.next_char.isalpha() or self.next_char == "_" or self.next_char.isdigit())):
                self.get_next_char()
                bufferList.append(self.current_char)
                
            buffer = "".join(bufferList)
            token = self._identifiers.get(buffer)
            if(token == None):
                token = token_type.IDENTIFIER
                return Token(token, buffer, self.input.get_location())
            else:
                return Token(token, buffer, self.input.get_location())

        elif (self.current_char and (self.current_char.isdigit())):
            bufferList = []
            bufferList.append(self.current_char)

            while(self.next_char and self.next_char.isdigit()):
                self.get_next_char()
                bufferList.append(self.current_char)
            
            if(self.next_char == "."):
                self.get_next_char()
                bufferList.append(self.current_char)

            while(self.next_char and self.next_char.isdigit()):
                self.get_next_char()
                bufferList.append(self.current_char)

            buffer = "".join(bufferList)
            token = token_type.NUMBER_LITERAL
            return Token(token, buffer, self.input.get_location())
        
        elif(self.current_char in self._simple_tokens):
            return Token(self._simple_tokens.get(self.current_char), self.current_char)

        elif(self.current_char == '"'):
            bufferList = []
            bufferList.append(self.current_char)
            done = False
            while(done is False):
                if(self.next_char == '$'):
                    self.get_next_char()
                    if(self.next_char == '"'):
                        self.get_next_char()
                        bufferList.append(self.current_char)
                    else:
                        bufferList.append(self.current_char)
                if(self.next_char is None):
                    raise QuoteError("Missing quote")
                if(self.next_char == '"'):
                    done = True
                self.get_next_char()
                bufferList.append(self.current_char)

            buffer = "".join(bufferList)
            token = token_type.QUOTE
            return Token (token, buffer, self.input.get_location())

        else:
            bufferList = []
            bufferList.append(self.current_char)

            if(self.current_char == "|"):
                if(self.next_char == "|"):
                    self.get_next_char()
                    bufferList.append(self.current_char)

            elif(self.current_char == "&"):
                if(self.next_char == "&"):
                    self.get_next_char()
                    bufferList.append(self.current_char)

            elif(self.current_char == "=" or self.current_char == "!"):
                if(self.next_char == "="):
                    self.get_next_char()
                    bufferList.append(self.current_char)

            elif(self.current_char == "<" or self.current_char == ">"):
                if(self.next_char == "="):
                    self.get_next_char()
                    bufferList.append(self.current_char)

            else:
                buffer = "".join(bufferList)
                return Token(token_type.INVALID, buffer, self.input.get_location())

            buffer = "".join(bufferList)
            token = self._complex_tokens.get(buffer)

            if(token == None):
                return Token(token_type.INVALID, buffer, self.input.get_location())
            return Token(token, buffer, self.input.get_location())