from Parser import Parser
from get_input import  string_buffer
from Lexer import Lexer
from Parser import Parser


def main():
    pass

def test_object():
    s = string_buffer("object = {}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].name == "object"

def test_object_inside_function():
    s = string_buffer("object = {fun function(a){}}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].name == "object"
    assert result.elements[0].elements[0].name == "function"

def test_object_inside_variable_assignment():
    s = string_buffer("object = {var = 5}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].name == "object"


def test_function_definition():
    s = string_buffer("fun function() {}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_block_funcall():
    s = string_buffer("fun function() {cal()}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_block_var_assign():
    s = string_buffer("fun function() {var = 4}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_block_if():
    s = string_buffer("fun function() {if(a){}}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_block_while():
    s = string_buffer("fun function() {while(a){}}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_block_foreach():
    s = string_buffer("fun function() {foreach(a b){}}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)
    assert result.elements[0].name == "function"

def test_value_assignment():
    s = string_buffer("ścieżka = 32")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].name.operands[0].name == "ścieżka"
    assert result.elements[0].expression.and_conditions[0].relations[0].algebraic_expressions[0].factors[0].values[0].value == "32"

def test_function_call_simple():
    s = string_buffer("function()")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].operands[0].name == "function"

def test_if():
    s = string_buffer("if(condition){}else{}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].condition.and_conditions[0].relations[0].algebraic_expressions[0].factors[0].values[0].operands[0].name == "condition"

def test_while():
    s = string_buffer("while(condition){}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].condition.and_conditions[0].relations[0].algebraic_expressions[0].factors[0].values[0].operands[0].name == "condition"

def test_foreach():
    s = string_buffer("foreach(name name2){}")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

    assert result.elements[0].param[0] == "name"
    assert result.elements[0].param[1] == "name2"

def test_expression_add():
    s = string_buffer("zmienna = 1 + 2 - 4")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_multiply():
    s = string_buffer("zmienna = 1 * 2 / 4")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_AND():
    s = string_buffer("zmienna = a && b")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_OR():
    s = string_buffer("zmienna = a || b")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_relation():
    s = string_buffer("zmienna = a >= b < 4")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_equality():
    s = string_buffer("zmienna = a == 4")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_brackets():
    s = string_buffer("zmienna = (a + 4) * b")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_mixed():
    s = string_buffer("zmienna = (a + 4) * b || 5 && 1 >= t")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_expression_funcall():
    s = string_buffer("zmienna = b()")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_funcall_with_args():
    s = string_buffer("zmienna = b(a,b)")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_funcall_with_args_complex():
    s = string_buffer("zmienna = b(a,b).x.e(p)")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)

def test_variable_complex():
    s = string_buffer("zmienna = b(a,b).x.e(p).q")
    l = Lexer(s)
    p = Parser(l)

    result = p.parse_program()
    print(result)


if __name__ == "__main__":
    main()