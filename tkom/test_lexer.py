from Lexer import Lexer
from get_input import string_buffer
from Token import token_type


def main():
    pass

if __name__ == "__main__":
    main()

def test_init_variable():
    s = string_buffer("żmienna = zmienna1")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.ASSIGNMENT
    assert l.next_token().type == token_type.IDENTIFIER

def test_init_variable_algebraic_expression():
    s = string_buffer("zmienna3 = 5 + 3")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.ASSIGNMENT
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.ADD_OPERATOR
    assert l.next_token().type == token_type.NUMBER_LITERAL
        

def test_function_definition():
    s = string_buffer("fun a(var, var){}")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.FUNCTION
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.COMA
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE

def test_algebraic_expression():
    s = string_buffer("(4+3)*5-2.3")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.ADD_OPERATOR
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.MUL_OPERATOR
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.ADD_OPERATOR
    assert l.next_token().type == token_type.NUMBER_LITERAL

def test_object_name():
    s = string_buffer("obj.obj().a")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.DOT
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.DOT
    assert l.next_token().type == token_type.IDENTIFIER


def test_logic_operators():
    s = string_buffer("|| && != == <= >= < > ! | & ")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.OR
    assert l.next_token().type == token_type.AND
    assert l.next_token().type == token_type.EQUALITY_OPERATOR
    assert l.next_token().type == token_type.EQUALITY_OPERATOR
    assert l.next_token().type == token_type.COMP_OPERATOR
    assert l.next_token().type == token_type.COMP_OPERATOR
    assert l.next_token().type == token_type.COMP_OPERATOR
    assert l.next_token().type == token_type.COMP_OPERATOR
    assert l.next_token().type == token_type.NEGATION
    assert l.next_token().type == token_type.INVALID
    assert l.next_token().type == token_type.INVALID
        

def test_if():
    s = string_buffer("if(w > 5){} ")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.IF
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.COMP_OPERATOR
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE

def test_while():
    s = string_buffer("while(true){}")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.WHILE
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE

def test_foreach():
    s = string_buffer("foreach(temp_sensor){}")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.FOREACH
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE

def test_fun_on():
    s = string_buffer("fun x() on condition1{}")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.FUNCTION
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.ON
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE

def test_fun_each():
    s = string_buffer("fun x () each 10 ms {}")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.FUNCTION
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.BRACKET_OPEN
    assert l.next_token().type == token_type.BRACKET_CLOSE
    assert l.next_token().type == token_type.EACH
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.PARENTH_OPEN
    assert l.next_token().type == token_type.PARENTH_CLOSE


def test_time_unit():
    s = string_buffer("ms s m h d mss sm hd")
    l = Lexer(s)
    
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.TIME_DESCRIBER
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.IDENTIFIER
    assert l.next_token().type == token_type.IDENTIFIER
    
        
def test_create():
    s = string_buffer("")
    l = Lexer(s)
    assert s

def test_EOF():
    s = string_buffer("")
    l = Lexer(s)
    assert l.next_token().type == token_type.END_OF_FILE

def test_digit():
    s = string_buffer("1111 111.111 111.111.111")
    l = Lexer(s)
        
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.NUMBER_LITERAL
    assert l.next_token().type == token_type.DOT
    assert l.next_token().type == token_type.NUMBER_LITERAL


def test_quote():
    s = string_buffer('"aa" "aa$""')
    l = Lexer(s)
        
    assert l.next_token().type == token_type.QUOTE
    assert l.next_token().type == token_type.QUOTE


