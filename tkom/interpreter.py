from Lexer import Lexer
from Parser import Parser
from visitor import InterpreterVisitor

class Interpreter:
    def __init__(self, parser : Parser):
        self.parser = parser
        
    def execute(self):
        program = self.parser.parse_program()
        return program.accept(InterpreterVisitor())