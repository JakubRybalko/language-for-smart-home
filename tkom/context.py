from __future__ import annotations
from typing import List
from node_classes import FunctionCall, FunctionDefinition, Visitor
from Exceptions import RuntimeError
from abc import ABC, abstractmethod
import random


class Observer(ABC):
    @abstractmethod
    def update(self, subject: Subject) -> None:
        pass

class Subject(ABC):

    @abstractmethod
    def attach(self, observer: Observer) -> None:
        pass

    @abstractmethod
    def detach(self, observer: Observer) -> None:
        pass

    @abstractmethod
    def notify(self) -> None:
        pass
    

class Context(Subject):
    def __init__(self, parent_context=None):
        self.parent : ObjectContext = parent_context
        self.elements = {}
        self.observers = []

    def define_element(self, name, value):
        """define new element or redefine old one
        """
        self.check_redefinition(name)

        if not self.elements.get(name):
            if context := self.element_exist_in_parents(name):
                context.define_element(name, value)
                return
        self.elements[name] = value

        self.notify()

    def define_element_in_this_context(self, name, value):

        self.elements[name] = value
        self.notify()

    def get_element(self, name):
        result = self.elements.get(name)

        if result is None and self.parent:
            return self.parent.get_element(name)
        else:
            return result

    def get_possible_object(self, name):
        result = self.elements.get(name)

        if result is None and self.parent:
            return self.parent.get_possible_object(name)
        else:
            return result

    def get_definition(self, name):
        if self.parent:
            return self.parent.get_definition(name)
        return None

    def get_parent_context(self):
        if self.parent:
            return self.parent

    def check_redefinition(self, name):
        if name in self.elements.keys():
            return
        if self.parent:
            self.parent.check_redefinition(name)

    def element_exist_in_parents(self, name):
        if self.elements.get(name) is not None:
            return self
        elif self.parent:
            return self.parent.element_exist_in_parents(name)
        return None

    def check_element_defined(self, name):
        if self.elements.get(name) is not None:
            return self
        elif self.parent:
            return self.parent.check_element_defined(name)
        raise RuntimeError("Missing declaration of element")

    def get_global_context(self):
        if self.parent:
            return self.parent.get_global_context()
        return self

    def __str__(self):
        ret = "CONTEXT:\n"
        ret += "\nElements:\n"
        for name, value in self.elements.items():
            ret += "\t" + name + " = " + str(value) + "\n"
        return ret

    def attach(self, observer: Observer) -> None:
        self.observers.append(observer)

    def detach(self, observer: Observer) -> None:
        self.observers.remove(observer)

    def notify(self) -> None:
        for observer in self.observers:
            observer.update(self)


    

class ObjectContext(Context):
    def __init__(self, parent_context=None):
        super().__init__(parent_context)
        self.definitions = {}

    def define_element(self, name, value):
        if name in self.definitions.keys():
            raise RuntimeError("Redefinition of element")
        if name == 'return':
            raise RuntimeError("Invalid use of return")

        self.elements[name] = value

        self.notify()


    def define_definition(self, name, function_execution):
        
        self.check_redefinition_function(name)

        self.definitions[name] = function_execution
        

    def get_definition(self, name):
        result = self.definitions.get(name)

        if not result and self.parent:
            return self.parent.get_definition(name)
        else:
            return result


    def check_redefinition(self, name):
        if name in self.elements.keys():
            return
        if name in self.definitions.keys():
            raise RuntimeError("Redefinition of element")
        if self.parent:
            self.parent.check_redefinition(name)

    def check_redefinition_function(self, name):
        if name in self.definitions.keys() or name in self.elements.keys():
            raise RuntimeError("Redefinition of element")

    def check_element_defined(self, name):
        if self.elements.get(name) is not None or self.definitions.get(name) is not None:
            return self
        elif self.parent:
            return self.parent.check_element_defined(name)
        raise RuntimeError("Missing declaration of element")

    def __str__(self, depth = 0):
        ret = depth * "\t" + "CONTEXT:\n"  + depth * "\t" + "Definitions:\n"
        for name, value in self.definitions.items():
            ret += (depth + 1) * "\t" + name + "\n\n"
        ret += "\n" + depth * "\t" + "Elements:\n"
        for name, value in self.elements.items():
            ret += (depth + 1) * "\t" + name + " = " + str(value) + "\n"
        return ret


class GlobalContext(ObjectContext):
    def __init__(self):
        super().__init__()
        self.objects = {}
        self.__init_default_definitions()

    def __init_default_definitions(self):
        self.define_definition("return", None)
        self.define_definition("sensor", Sensor())
        self.define_definition("log", Log())
        self.define_definition("True", True)
        self.define_definition("False", False)
        pass


    def define_object(self, name, object_context : ObjectContext):
        self.check_redefinition(name)

        self.objects[name] = object_context

    def get_object(self, name):
        return self.objects.get(name)

    def get_possible_object(self, name):
        result = self.elements.get(name)
        if result is not None:
            return result

        result = self.objects.get(name)  
        if result is not None:
            return result

        return None  


    def check_redefinition(self, name):
        if name in self.elements.keys():
            return
        if name in self.objects.keys():
            raise RuntimeError("Redefinition of element")
        if name in self.definitions.keys():
            raise RuntimeError("Redefinition of element")

    def check_element_defined(self, name):
        if self.elements.get(name) is not None or self.definitions.get(name) is not None or self.objects.get(name) is not None:
            return self
        raise RuntimeError("Missing declaration of element")

    def __str__(self, depth=0):
        ret = "CONTEXT:\nObjects:\n"
        for name, value in self.objects.items():
            ret += "\t" + name + "\n" + value.__str__(depth+1) + "\n"
        ret += "\nDefinitions:\n"
        for name, value in self.definitions.items():
            ret += "\t" + name + "\n"
        ret += "\nElements:\n"
        for name, value in self.elements.items():
            ret += "\t" + name + " = " + str(value) + "\n"
        ret += "\nObservers = " + str(len(self.observers)) + "\n"
        return ret


class SpecialFunctions(ABC):
    @abstractmethod
    def execute(self, visitor , context : Context, element : FunctionCall) -> None:
        pass

class Sensor(SpecialFunctions):
    def __init__(self):
        self.connected_sensors = []
    
    def execute(self, visitor , context : Context, element : FunctionCall):
        iter=0
        name = ".sensor" + str(iter)
        while visitor.global_context.get_object(name) is not None:
            iter += 1
            name = ".sensor" + str(iter)
        
        new_context = ObjectContext(visitor.global_context)
        visitor.global_context.define_object(name, new_context)

        self.connected_sensors.append(name)

        new_context.elements["temp"] = random.randrange(-30,30,1)
        new_context.elements["open"] = True
        new_context.elements["battery_level"] = random.randrange(0,100,1)

        visitor.variables.append(new_context)

class Log(SpecialFunctions):

    def execute(self, visitor , context : Context, element : FunctionCall):
        arg = []

        for a in element.arguments:
            a.accept(visitor)
            arg.append(visitor.variables.pop())
        
        ret = "LOG:\t"
        for a in arg:
            ret += str(a) + "\t"

        print(ret)

        visitor.variables.append(self)
