from get_input import string_buffer
from Lexer import Lexer
from Parser import Parser
from interpreter import Interpreter
from context import ObjectContext


def main():
    s = string_buffer("a = 1 == 0")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    print(v.global_context.elements["a"] ) 

if __name__ == "__main__":
    main()

def test_assignment():
    s = string_buffer("a = 1 == 0")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["a"] == False

def test_assignment_complex():
    s = string_buffer("a = 1+3 b = a + 1 c = b")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["a"] == 4
    assert v.global_context.elements["b"] == 5
    assert v.global_context.elements["c"] == 5

def test_assignment_inside_object():
    s = string_buffer("a = {b=1} z = a.b")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["z"] == 1

def test_function_with_var_assignment():
    s = string_buffer("var = 1 fun f(){ var = var +  2 } f()")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["var"] == 3

def test_function_with_string():
    s = string_buffer('z = 0 fun f(zmienna){z = zmienna} f("string")')
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["z"] == '"string"'

def test_if():
    s = string_buffer("var = 1 fun f(){ if(1>0){var = 2}else{ var = 5} } f()")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["var"] == 2

def test_while():
    s = string_buffer("z = 2 fun a(x){while(x > 6){x = x - 1} z = x}  a(10)")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["z"] == 6

def test_foreach():
    s = string_buffer("ret = 0 fun temp(){return(1)} winda = {sensor = temp()} hol = {sensor = temp()}  foreach(sensor x){ret = ret + x}")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["ret"] == 2

def test_return():
    s = string_buffer("x = 2 fun a(value){return(value+5)} x = a(x)")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["x"] == 7

def test_return_with_expression():
    s = string_buffer("fun cal(){return(1*2*3)} zmienna = 1 + cal() + 2 + 3  ")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["zmienna"] == 12

def test_recursion():
    s = string_buffer("fun recur(value){if(value>1){return(value * recur(value-1))}else{return(value)}} x = recur(5)")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["x"] == 120

def test_object_assignment():
    s = string_buffer("a = {} b = a")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert isinstance( v.global_context.elements["b"], ObjectContext)

def test_function_assignment():
    s = string_buffer("fun a() {} b = a b()")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()


def test_function_redefinition_inside_object():
    s = string_buffer("a = {fun sensor(){}}")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.objects["a"].definitions["sensor"]

def test_function_with_on():
    s = string_buffer('count = 0 var = 1 fun x() on 5>var{count = count + 1} var = 4')
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["count"] == 1

# def test_function_with_each():
#     s = string_buffer('a = 0 fun x() each 1 s {a = a + 1 log(a)}')
#     l = Lexer(s)
#     p = Parser(l)
#     i = Interpreter(p)
#     i.execute()

def test_log():
    s = string_buffer('log("string", "drugi string", 1, 2)')
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    i.execute()

def test_sensor():
    s = string_buffer('a = {b = sensor() c = sensor()} log(a.b.temp, a.c.temp)')
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.objects[".sensor0"].elements["open"] == True
    assert v.global_context.objects["a"].elements["b"].elements["open"] == True

def test_function_call_returning_object():
    s = string_buffer(" b = {c=5} fun a(){return(b)} x = a().c")
    l = Lexer(s)
    p = Parser(l)
    i = Interpreter(p)
    v = i.execute()

    assert v.global_context.elements["x"] == 5