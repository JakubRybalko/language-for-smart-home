class get_input:
    def get_char(self):
        raise NotImplementedError
    
    def get_location(self):
        raise NotImplementedError

class string_buffer(get_input):
    def __init__(self, string):
        self.input = string
        self.gen = self.next_char_generator()
        self.location = -1

    def next_char_generator(self):
        for char in self.input:
            yield char
            self.location += 1

    def get_char(self):
        try:
            tmp = next(self.gen)
        except StopIteration:
            tmp = None
        return tmp

    def get_location(self):
        return self.location