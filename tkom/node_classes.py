from __future__ import annotations
from Token import Location
from abc import ABC, abstractmethod

class Component:
    @abstractmethod
    def accept(self, visitor: Visitor) -> None:
        pass

class Dog:
    pass
class Statement(Component):
    def __init__(self, loc: Location):
        self.location = loc
    
    def accept(self, visitor: Visitor):
        visitor.visit_statement(self)

class Program(Component):
    def __init__(self, elements: list=[]):
        self.objects_definitions_list = []
        self.statements = []
        self.functions_definitions = []
        self.elements = elements

    def __str__(self):
        ret = ""
        if(self.objects_definitions_list):
            ret = "Object definitions:\n"
            for d in self.objects_definitions_list:
                ret += d.__str__(1)
        if(self.statements):
            ret += "Statements:\n"
            for d in self.statements:
                ret += d.__str__(1)
        if (self.functions_definitions):
            ret += "Function definitions:\n"
            for d in self.functions_definitions:
                ret += d.__str__(1)
        return ret

    def accept(self, visitor: Visitor):
        return visitor.visit_program(self)

class ObjectDefinition(Component):
    def __init__(self, loc: Location, name: str, elements: list=[]):
        self.location = loc
        self.name = name
        self.value_assignments = []
        self.functions_definitions = []
        self.elements = elements

    def __str__(self, depth=0):
        ret = "\t" * depth + self.name + "\n"
        if(self.value_assignments):
            ret += "Value assignments:\n"
            for d in self.value_assignments:
                ret += d.__str__(2)
        if(self.functions_definitions):
            ret += "Function definitions:\n"
            for d in self.functions_definitions:
                ret += d.__str__(2)
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_object_definition(self)

class Expression(Statement):
    def __init__(self, loc: Location):
        super().__init__(loc)

    def get_value(self):
        raise NotImplementedError

    def accept(self, visitor: Visitor):
        visitor.visit_expression(self)


class AlgebraicExpression(Expression):
    def __init__(self, res_list: list, operator):
        super().__init__(0)
        self.factors = res_list
        self.operator = operator

    def __str__(self, depth=0):
        res = ""
        if self.factors is None:
            return res
        res +=  self.factors[0].__str__(depth + 1) + "\n"
        for d in range(1, len(self.factors)):
            res += "\t" * depth + self.operator[d-1] + "\n"
            res +=  self.factors[d].__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_algebraic_expression(self)


class Factor(Expression):
    def __init__(self, res_list: list, operator):
        super().__init__(0)
        self.values = res_list
        self.operator = operator

    def __str__(self, depth=0):
        res = ""
        if self.values is None:
            return res
        res +=  self.values[0].__str__(depth + 1) + "\n"
        for d in range(1, len(self.values)):
            res += "\t" * depth + self.operator[d-1] + "\n"
            res +=  self.values[d].__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_factor(self)

class Value(Expression):
    def __init__(self, loc: Location):
        super().__init__(loc)

    def accept(self, visitor: Visitor):
        visitor.visit_value(self)


class LogicalExpression(Expression):
    def __init__(self, res_list: list, operator):
        super().__init__(0)
        self.and_conditions = res_list
        self.operator = operator

    def __str__(self, depth=0):
        res = ""
        if self.and_conditions is None:
            return res
        res +=  self.and_conditions[0].__str__(depth + 1) + "\n"
        for d in range(1, len(self.and_conditions)):
            res += "\t" * depth + self.operator[d-1] + "\n"
            res +=  self.and_conditions[d].__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_logical_expression(self)


class Comparison(Expression):
    def __init__(self, res_list: list, operator):
        super().__init__(0)
        self.algebraic_expressions = res_list
        self.operator = operator

    def __str__(self, depth=0):
        res = ""
        if self.algebraic_expressions is None:
            return res
        res +=  self.algebraic_expressions[0].__str__(depth + 1) + "\n"
        for d in range(1, len(self.algebraic_expressions)):
            res += "\t" * depth + self.operator[d-1] + "\n"
            res +=  self.algebraic_expressions[d].__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_comparison(self)


class AndCondition(Expression):
    def __init__(self, res_list: list, operator):
        super().__init__(0)
        self.relations = res_list
        self.operator = operator

    def __str__(self, depth=0):
        res = ""
        if self.relations is None:
            return res
        res +=  self.relations[0].__str__(depth + 1) + "\n"
        for d in range(1, len(self.relations)):
            res += "\t" * depth + self.operator[d-1] + "\n"
            res +=  self.relations[d].__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_and_condition(self)



class IdValue(Value):
    def __init__(self,
                 loc: Location,
                 name: str = None,
                 neg_op=None,
                 operands=None):
        super().__init__(loc)
        self.name = name
        self.neg_op = neg_op
        if operands:
            self.operands = operands
        else:
            self.operands = []

    def __str__(self, depth=0):
        res = ""
        res += "\t" * depth +  self.name + "\n"
        for op in reversed(self.operands):
            res += op.__str__(depth + 1) + "\n"
            depth += 1
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_id_value(self)


class ConstValue(Value):
    def __init__(self, loc: Location, value, neg_op=None):
        super().__init__(loc)
        self.value = value
        self.neg_op = neg_op

    def __str__(self, depth=0):
        return "\t" * depth + str(self.value) + "\n"

    def get_value(self):
        if not self.neg_op:
            return self.value
        else:
            raise NotImplementedError

    def accept(self, visitor: Visitor):
        visitor.visit_const_value(self)


class Block(Component):
    def __init__(self, statements: list):
        self.statements = statements

    def __str__(self, depth=0):
        res = depth * "\t" + "BLOCK\n"
        for statement in self.statements:
            res += statement.__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_block(self)

class FunctionDefinition(Component):
    def __init__(
        self,
        loc: Location,
        name: str,
        block: Block,
        arguments: list = None,
        on_condition: LogicalExpression = None,
        each_expression: AlgebraicExpression = None,
        time_describer: str = None,
    ):
        self.location = loc
        self.name = name

        if arguments:
            self.arguments = arguments
        else:
            self.arguments = []
        
        self.on_condition = on_condition

        self.each_expression = each_expression

        self.time_describer = time_describer

        self.block = block

    def __str__(self, depth=0):
        ret = "\t" * depth + f"FUN: {self.name}\n"  + "\t" * depth
        for x in self.arguments:
            ret += x
        ret += "\n"
        for x in self.on_condition:
            ret += "\t" * depth + "ON" + "\n"
            ret += x.__str__(depth + 1)
        for x in self.each_expression:
            ret += "\t" * depth + "EACH" + "\n"
            ret += x.__str__(depth + 1)
            if self.time_describer:
                ret += "\t" * depth + "Time unit: " + self.time_describer +  "\n"
        ret += self.block.__str__(depth + 1)
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_function_definition(self)


class FunctionCall(Value):
    def __init__(self, 
                loc: Location,
                name: str, 
                arguments: list,
                neg_op = None):
        self.arguments = arguments
        self.name = name
        self.neg_op = neg_op
        super().__init__(loc)
        
    def __str__(self, depth=0):
        ret = "\n"
        ret += "\t" * depth + self.name +  " FunctionCall"+ "()"
        for arg in reversed( self.arguments ):
            ret += arg.__str__(depth + 1)
        ret += "\n"
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_function_call(self)

class FunctionCallOrValue(Value):
    def __init__(self, 
                loc: Location, 
                operands: list):
        super().__init__(loc)
        self.operands = operands
        if issubclass(type(operands[-1]), FunctionCall):
            self.is_function_call = True;
        else:
            self.is_function_call = False

    def get_is_fun_call(self):
        return self.is_function_call
        
    def __str__(self, depth=0):
        ret = "\n"
        for arg in reversed( self.operands ):
            ret += arg.__str__(depth + 1)
            depth += 1
        ret += "\n"
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_function_call_or_value(self)

class IfStatement(Statement):
    def __init__(self,
                 loc: Location,
                 condition: LogicalExpression,
                 true_block: Block,
                 false_block: Block = None):
        super().__init__(loc)
        self.condition = condition
        self.true_block = true_block
        self.false_block = false_block

    def __str__(self, depth=0):
        ret = "\t" * depth + "IF\n"
        ret += self.condition.__str__(depth + 1)
        ret += self.true_block.__str__(depth + 1)
        if self.false_block:
            ret += "\t" * depth + "ELSE\n"
            ret += self.false_block.__str__(depth + 1)
        else:
            ret += "\t" * depth + "None else\n"
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_if_statement(self)

class WhileStatement(Statement):
    def __init__(self, loc: Location, condition: LogicalExpression,
                 block: Block):
        super().__init__(loc)
        self.condition = condition
        self.block = block

    def __str__(self, depth=0):
        ret = "\t" * depth + "While cond:"
        ret += self.condition.__str__(depth + 1)
        ret += self.block.__str__(depth + 1)
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_while_statement(self)


class ForeachStatement(Statement):
    def __init__(self, loc: Location, param: list,
                 block: Block):
        super().__init__(loc)
        self.param = param
        self.block = block

    def __str__(self, depth=0):
        ret = "\t" * depth + "Foreach param:"
        for arg in self.param:
            ret +=  "\t" * depth  + arg
        ret += "\n" +  self.block.__str__(depth + 1)
        return ret

    def accept(self, visitor: Visitor):
        visitor.visit_foreach_statement(self)


class ValueAssignment(Statement):
    def __init__(self, name: FunctionCallOrValue, expression: Expression):
        super().__init__(name.location)
        self.name = name
        self.expression = expression

    def __str__(self, depth=0):
        res = self.name.__str__(depth)
        res += depth * "\t" + "=\n"
        res += self.expression.__str__(depth + 1)
        return res

    def accept(self, visitor: Visitor):
        visitor.visit_value_assignment(self)




class Visitor(ABC):
    
    @abstractmethod
    def visit_program(self, element: Program) -> None:
        pass

    @abstractmethod
    def visit_statement(self, element: Statement) -> None:
        pass

    @abstractmethod
    def visit_object_definition(self, element: ObjectDefinition) -> None:
        pass

    @abstractmethod
    def visit_expression(self, element: Expression) -> None:
        pass

    @abstractmethod
    def visit_algebraic_expression(self, element: AlgebraicExpression) -> None:
        pass

    @abstractmethod
    def visit_factor(self, element: Factor) -> None:
        pass

    @abstractmethod
    def visit_value(self, element: Value) -> None:
        pass

    @abstractmethod
    def visit_logical_expression(self, element: LogicalExpression) -> None:
        pass

    @abstractmethod
    def visit_comparison(self, element: Comparison) -> None:
        pass

    @abstractmethod
    def visit_and_condition(self, element: AndCondition) -> None:
        pass

    @abstractmethod
    def visit_id_value(self, element: IdValue) -> None:
        pass

    @abstractmethod
    def visit_const_value(self, element: ConstValue) -> None:
        pass

    @abstractmethod
    def visit_block(self, element: Block) -> None:
        pass

    @abstractmethod
    def visit_function_definition(self, element: FunctionDefinition) -> None:
        pass

    @abstractmethod
    def visit_function_call(self, element: FunctionCall) -> None:
        pass

    @abstractmethod
    def visit_function_call_or_value(self, element: FunctionCallOrValue) -> None:
        pass

    @abstractmethod
    def visit_if_statement(self, element: IfStatement) -> None:
        pass

    @abstractmethod
    def visit_while_statement(self, element: WhileStatement) -> None:
        pass

    @abstractmethod
    def visit_foreach_statement(self, element: ForeachStatement) -> None:
        pass

    @abstractmethod
    def visit_value_assignment(self, element: ValueAssignment) -> None:
        pass

