from Token import Location, Token, token_type
from Exceptions import ParserError
from Lexer import Lexer
from node_classes import FunctionCallOrValue, LogicalExpression, Statement, Expression, ValueAssignment, AlgebraicExpression, Factor, Value, Comparison, AndCondition,  FunctionCall, IdValue, ConstValue, Block, IfStatement, WhileStatement, FunctionDefinition, Program, ObjectDefinition, ForeachStatement

class Parser:

    def __init__(self, lexer):
        self.token : Token = None
        self.lexer = lexer
        self.token_next : Token = self.lexer.next_token()

    def get_next_token(self, str: str = "") -> Token:
        # self.token = self.lexer.next_token()
        self.token = self.token_next
        self.token_next = self.lexer.next_token()
        if(self.token.value == token_type.INVALID):
            raise ParserError("Invalid sign!")
        return self.token

    def parse(self):
        self.get_next_token()
        tree = self.parse_program()




    def parse_program(self) -> Program: 
        self.get_next_token()
        elements = []

        while(self.token.type != token_type.END_OF_FILE):
            if (self.token.type == token_type.IDENTIFIER or self.token.type == token_type.FUNCTION):
                if p := self.parse_nested_name_global():
                    elements.append(p)
            else:
                if p := self.parse_conditional_statement():
                    elements.append(p)
                else:
                    raise ParserError("Invalid sign, program loop")
        return Program(elements)


    def read_param(self):
        param = []
        if(self.token.type != token_type.IDENTIFIER):
            return param
        param.append(self.token.value)
        self.get_next_token()

        while(self.token.type == token_type.COMA):
            self.get_next_token()
            if(self.token.type != token_type.IDENTIFIER):
                raise ParserError("Invalid param")
                

            param.append(self.token.value)
            self.get_next_token()

        return param
        


    
    def parse_function(self,) -> FunctionDefinition: 
        param = []
        on_expression = []
        each_expression = []
        time_describer = None

        location = self.token.location
        name = self.token.value

        if(self.token.type != token_type.IDENTIFIER):
            raise ParserError("Invalid function declaration, wrong function name")
        self.get_next_token()

        if(self.token.type != token_type.BRACKET_OPEN):
            raise ParserError("Invalid function declaration, no (")
        self.get_next_token()

        param = self.read_param()
        
        if(self.token.type != token_type.BRACKET_CLOSE):
            raise ParserError("Missing ) in function declaration")
        self.get_next_token()


        if(self.token.type == token_type.ON):
            self.get_next_token()
            on_expression.append(self.parse_logical_expression())
        elif(self.token.type == token_type.EACH):
            self.get_next_token()
            each_expression.append(self.parse_algebraic_expression())
            if(self.token.type == token_type.TIME_DESCRIBER):
                time_describer = self.token.value
                self.get_next_token()
            else:
                raise ParserError("No time describer")

        block = self.parse_block()

        return FunctionDefinition(location, name, block, param, on_expression, each_expression, time_describer)





    def parse_instruction(self):  
        if(self.token.type == token_type.IDENTIFIER):
            return self.parse_nested_name_inside_function()
        elif(self.token.type == token_type.FOREACH):
            return self.parse_foreach_statement()
        return self.parse_conditional_statement()
        
        


    def parse_nested_name_global(self):  
        if(self.token.type == token_type.FUNCTION):
            self.get_next_token()
            return self.parse_function()
        ret = self.parse_function_call_or_id_value()
        if(len(ret.operands) > 1):
            if ret.get_is_fun_call() is False:
                if(self.token.type == token_type.ASSIGNMENT):
                    self.get_next_token()
                    return self.parse_rest_value_assignment(ret)
            return ret 

        else:
            if ret.get_is_fun_call() is False:
                if(self.token.type == token_type.ASSIGNMENT):
                    self.get_next_token()
                    if(self.token.type == token_type.PARENTH_OPEN):
                        self.get_next_token()
                        return self.parse_rest_object(ret.operands[0])
                    return self.parse_rest_value_assignment(ret)
            return ret 






    def parse_nested_name_inside_object(self):  
        if(self.token.type == token_type.FUNCTION):
            self.get_next_token()
            return self.parse_function()
        name = self.token.value
        location = self.token.location
        
        ret = self.parse_function_call_or_id_value()

        if(self.token.type != token_type.ASSIGNMENT):
            raise ParserError("Missing assgnment sign")
        self.get_next_token()

        if ret.get_is_fun_call() is False:
            return self.parse_rest_value_assignment(ret)

        return None
        





    def parse_nested_name_inside_function(self):  

        ret = self.parse_function_call_or_id_value()
        
        if(self.token.type == token_type.ASSIGNMENT):
            self.get_next_token()
            return self.parse_rest_value_assignment(ret)

        if ret.get_is_fun_call():
            return ret
        
        raise ParserError("Invalid variable declaration or funcall")





    def parse_rest_value_assignment(self, name) -> ValueAssignment:
        return ValueAssignment(name, self.parse_logical_expression())




    def parse_rest_object(self, name: IdValue) -> ObjectDefinition: 
        elements = []
        # object_definition = ObjectDefinition(name.location, name.name)
        
        while self.token.type == token_type.IDENTIFIER or self.token.type == token_type.FUNCTION :
            # object_definition.add_element(self.parse_nested_name_inside_object())
            elements.append(self.parse_nested_name_inside_object())
            


        if (self.token.type != token_type.PARENTH_CLOSE):
            raise ParserError("No } after creating object")
        self.get_next_token()

        return ObjectDefinition(name.location, name.name, elements);



    def parse_function_call_or_id_value(self, neg_op = None):
        operands = []
        arguments = []

        is_id_value = True

        location = self.token.location
        location_ret = self.token.location
        name = self.token.value

        self.get_next_token()

        if(self.token.type == token_type.BRACKET_OPEN):
            is_id_value = False
            self.get_next_token()
            if(self.token.type == token_type.QUOTE):
                arguments.append(IdValue(self.token.location ,self.token.value))
                self.get_next_token()
            elif(self.token.type == token_type.IDENTIFIER or self.token.type == token_type.NUMBER_LITERAL):
                if p := self.parse_logical_expression():
                    arguments.append(p)

            while (self.token.type == token_type.COMA):
                self.get_next_token()
                if(self.token.type == token_type.QUOTE):
                    arguments.append(IdValue(self.token.location ,self.token.value))
                    self.get_next_token()
                elif(self.token.type == token_type.IDENTIFIER or self.token.type == token_type.NUMBER_LITERAL):
                    if p := self.parse_logical_expression():
                        arguments.append(p)
                else:
                    raise ParserError("No arguments after ,")

            if(self.token.type != token_type.BRACKET_CLOSE):
                raise ParserError("No ) after param of function call")
            self.get_next_token()
            
            operands.append(FunctionCall(location, name, arguments))
        else:
            operands.append(IdValue(location, name))

        while(self.token.type == token_type.DOT):
            is_id_value = True
            self.get_next_token()
            arguments = []
            location = self.token.location
            name = self.token.value

            
            if(self.token.type != token_type.IDENTIFIER):
                raise ParserError("Wrong name in function call or variable name")
            self.get_next_token()

            if(self.token.type == token_type.BRACKET_OPEN):
                is_id_value = False
                self.get_next_token()
                if(self.token.type == token_type.QUOTE):
                    arguments.append(IdValue(self.token.location ,self.token.value))
                    self.get_next_token()
                elif(self.token.type == token_type.IDENTIFIER or self.token.type == token_type.NUMBER_LITERAL):
                    if p := self.parse_logical_expression():
                        arguments.append(p)

                while (self.token.type == token_type.COMA):
                    self.get_next_token()
                    if(self.token.type == token_type.QUOTE):
                        arguments.append(IdValue(self.token.location ,self.token.value))
                        self.get_next_token()
                    elif(self.token.type == token_type.IDENTIFIER or self.token.type == token_type.NUMBER_LITERAL):
                        if p := self.parse_logical_expression():
                            arguments.append(p)
                    else:
                        raise ParserError("No arguments after ,")

                if(self.token.type != token_type.BRACKET_CLOSE):
                    raise ParserError("No ) after param of function call")
                self.get_next_token()
                operands.append(FunctionCall(location, name, arguments)) 
            else:
                operands.append(IdValue(location, name)) 

        return FunctionCallOrValue(location_ret, operands);
       







    def parse_conditional_statement(self) -> Statement:
        if(self.token.type == token_type.IF):
            return self.parse_if_statement()

        elif(self.token.type == token_type.WHILE):
            return self.parse_while_statement()

        elif(self.token.type == token_type.FOREACH):
            return self.parse_foreach_statement()

        return None





    def parse_if_statement(self) -> IfStatement:

        location = self.token.location
        self.get_next_token()

        if(self.token.type != token_type.BRACKET_OPEN):
            raise ParserError("Invalid if statement, no ( sign")
        self.get_next_token()

        if(self.token.type == token_type.BRACKET_CLOSE):
            raise ParserError("No condition")

        expr = self.parse_logical_expression()

        if(self.token.type != token_type.BRACKET_CLOSE):
            raise ParserError("Invalid if statement, no ) sign")
        self.get_next_token()

        block = self.parse_block()

        if(self.token.type == token_type.ELSE):
            self.get_next_token()
            block_else = self.parse_block()
            return IfStatement(location, expr, block, block_else)

        return IfStatement(location, expr, block)

        




    def parse_while_statement(self) -> WhileStatement:
        instructions = []

        location = self.token.location
        self.get_next_token()

        if(self.token.type != token_type.BRACKET_OPEN):
            raise ParserError("Invalid while statement, no ( sign")
        self.get_next_token()

        if(self.token.type == token_type.BRACKET_CLOSE):
            raise ParserError("No condition")

        expr = self.parse_logical_expression()

        if(self.token.type != token_type.BRACKET_CLOSE):
            raise ParserError("Invalid while statement, no ) sign")
        self.get_next_token()

        block = self.parse_block()

        return WhileStatement(location, expr, block)





    def parse_foreach_statement(self) -> ForeachStatement:
        param = []

        location = self.token.location
        self.get_next_token()

        if(self.token.type != token_type.BRACKET_OPEN):
            raise ParserError("Invalid foreach statement, no ( sign")
        self.get_next_token()

        if(self.token.type != token_type.IDENTIFIER):
            raise ParserError("Invalid foreach statement, wrong param name")
        param.append(self.token.value)
        self.get_next_token()

        if(self.token.type == token_type.DOT or self.token.type == token_type.BRACKET_OPEN):
            raise ParserError("Invalid param name")

        if(self.token.type != token_type.IDENTIFIER):
            raise ParserError("Invalid foreach statement, wrong param name")
        param.append(self.token.value)
        self.get_next_token()

        if(self.token.type == token_type.DOT or self.token.type == token_type.BRACKET_OPEN):
            raise ParserError("Invalid param name")

        if(self.token.type != token_type.BRACKET_CLOSE):
            raise ParserError("Invalid foreach statement, no ) sign")
        self.get_next_token()

        block = self.parse_block()

        return ForeachStatement(location, param, block);





    def parse_block(self) -> Block:

        if(self.token.type != token_type.PARENTH_OPEN):
            raise ParserError("Invalid statement, no { sign")
        self.get_next_token()

        statements = []
        while p := self.parse_instruction():
                statements.append(p)
        if(self.token.type != token_type.PARENTH_CLOSE):
            raise ParserError("Invalid statement, no } sign")
        self.get_next_token()

        return Block(statements)



    def parse_logical_expression(self):

        res_list = []
        operators = []
        res_list.append(self.parse_and_expression())
        while(self.token.type == token_type.OR):
            
            operators.append (self.token.value)
            self.get_next_token()
            res_list.append(self.parse_and_expression())
        return LogicalExpression(res_list, operators)




    def parse_and_expression(self) -> AndCondition:
        res_list = []
        operators = []
        res_list.append(self.parse_relational_expression())
        while(self.token.type == token_type.AND):
            
            operators.append (self.token.value)
            self.get_next_token()
            res_list.append(self.parse_relational_expression())
        return AndCondition(res_list, operators)




    def parse_relational_expression(self) -> Comparison:
        res_list = []
        operators = []
        
        res_list.append(self.parse_algebraic_expression())
        while(self.token.type == token_type.COMP_OPERATOR or self.token.type == token_type.EQUALITY_OPERATOR):
            
            operators.append (self.token.value)
            self.get_next_token()
            res_list.append(self.parse_algebraic_expression())
        return Comparison(res_list, operators)




    def parse_algebraic_expression(self) -> Expression:

        res_list = []
        operators = []
        res_list.append(self.parse_factor())
        while(self.token.type == token_type.ADD_OPERATOR):
            
            operators.append (self.token.value)
            self.get_next_token()
            res_list.append(self.parse_factor())
        return AlgebraicExpression(res_list, operators)






    def parse_factor(self) -> Factor:

        res_list = []
        operators = []

        res_list.append(self.parse_value())
        while(self.token.type == token_type.MUL_OPERATOR):
                
            operators.append (self.token.value)
            self.get_next_token()
            res_list.append(self.parse_value())

        return Factor(res_list, operators)






    def parse_value(self) -> Value:
        if self.token.type == token_type.NUMBER_LITERAL:
            location = self.token.location
            value = self.token.value
            self.get_next_token()
            return ConstValue(location, value)
        elif self.token.type in [ token_type.NEGATION, token_type.ADD_OPERATOR]:
            unary_token_value = self.token.value
            self.get_next_token()
            if(self.token.type == token_type.NUMBER_LITERAL):
                location = self.token.location
                value = self.token.value
                self.get_next_token()
                return ConstValue(location, value, unary_token_value)
            if self.token.type == token_type.IDENTIFIER:
                return self.parse_function_call_or_id_value(unary_token_value)
            raise ParserError('Wrong token after unary operation')

        elif self.token.type == token_type.IDENTIFIER:
            return self.parse_function_call_or_id_value()

        elif self.token.type == token_type.BRACKET_OPEN:
            self.get_next_token()
            
            res = self.parse_logical_expression()
            if self.token.type != token_type.BRACKET_CLOSE:
                raise ParserError("Missing )")
            self.get_next_token()

            return res

        return None

