from __future__ import annotations
#from node_classes import Component
from node_classes import FunctionCallOrValue, LogicalExpression, Statement, Expression, ValueAssignment, AlgebraicExpression, Factor, Value, Comparison, AndCondition, FunctionCall, IdValue, ConstValue, Block, IfStatement, WhileStatement, FunctionDefinition, Program, ObjectDefinition, ForeachStatement, Visitor
from context import Context, ObjectContext, GlobalContext, Observer, Subject, SpecialFunctions
from Exceptions import RuntimeError
from twisted.internet import task, reactor


class InterpreterVisitor(Visitor):

    def __init__(self):
        self.global_context = GlobalContext()
        self.current_context = self.global_context
        self.variables = []
        self.id_value = None
        self.do_checking = True
        self.return_flag = False
        self.is_assign = False
        self.get_r_value = True

        self.is_collecting_contexts = False
        self.contexts_to_observe = []



    def visit_program(self, element: Program) -> None:
        for el in element.elements:
            el.accept(self)
            if len(self.variables) > 0:
                self.variables = []
        return self


    def visit_statement(self, element: Statement) -> None:
        pass


    def visit_object_definition(self, element: ObjectDefinition) -> None:
        context = self.current_context = ObjectContext(parent_context=self.global_context)

        for el in element.elements:
            el.accept(self)

        self.current_context = self.global_context

        self.current_context.define_object(element.name,context)


    def visit_expression(self, element: Expression) -> None:
        pass


    def visit_algebraic_expression(self, element: AlgebraicExpression) -> None:
        for el in element.factors:
            el.accept(self)

        index = len(self.variables) - ( len(element.operator) + 1 )

        for op in element.operator:
            tmp = self.variables.pop(index)
            self.check_string(tmp, self.variables[index])
            if op == "+":
                self.variables[index] = tmp + self.variables[index]
            elif op == "-":
                self.variables[index] = tmp - self.variables[index]


    def visit_factor(self, element: Factor) -> None:
        for el in element.values:
            el.accept(self)

        index = len(self.variables) - ( len(element.operator) + 1 )

        for op in element.operator:
            tmp = self.variables.pop(index)
            self.check_string(tmp, self.variables[index])
            if op == "*":
                self.variables[index] = tmp * self.variables[index]
            elif op == "/":
                self.variables[index] = tmp / self.variables[index]


    def visit_value(self, element: Value) -> None:
        pass


    def visit_logical_expression(self, element: LogicalExpression) -> None:
        for el in element.and_conditions:
            el.accept(self)

        index = len(self.variables) - ( len(element.operator) + 1 )

        for op in element.operator:
            tmp = self.variables.pop(index)
            self.check_string(tmp, self.variables[index])
            self.variables[index] = tmp or self.variables[index]



    def visit_comparison(self, element: Comparison) -> None:
        for el in element.algebraic_expressions:
            el.accept(self)

        index = len(self.variables) - ( len(element.operator) + 1 )

        for op in element.operator:
            tmp = self.variables.pop(index)
            self.check_string(tmp, self.variables[index])
            if op == "==":
                self.variables[index] = tmp == self.variables[index]
            elif op == "!=":
                self.variables[index] = tmp != self.variables[index]
            elif op == ">":
                self.variables[index] = tmp > self.variables[index]
            elif op == "<":
                self.variables[index] = tmp < self.variables[index]
            elif op == ">=":
                self.variables[index] = tmp >= self.variables[index]
            elif op == "<=":
                self.variables[index] = tmp <= self.variables[index]


    def visit_and_condition(self, element: AndCondition) -> None:
        for el in element.relations:
            el.accept(self)

        index = len(self.variables) - ( len(element.operator) + 1 )

        for op in element.operator:     
            tmp = self.variables.pop(index)
            self.check_string(tmp, self.variables[index])
            self.variables[index] = tmp and self.variables[index]


    def visit_id_value(self, element: IdValue) -> None:
        if '"' in element.name:
            self.variables.append(element.name)
            return

        if self.do_checking:
            self.current_context.check_element_defined(element.name)

        self.id_value = element.name
        value = self.current_context.get_element(element.name)
        if value is None:
            value = self.current_context.get_definition(element.name)
            if value is None:
                value = self.global_context.get_object(element.name)

        val_context = self.current_context
        if isinstance(value, str) and not ('"' in value):
            val_context = self.current_context.check_element_defined(value)
            value = val_context.elements.get(value)
            while isinstance(value, str):
                val_context = val_context.check_element_defined(value)
                value = val_context.elements.get(value)

        if self.is_collecting_contexts:
            self.contexts_to_observe.append(val_context)
        if self.get_r_value:
            self.variables.append(value)


    def visit_const_value(self, element: ConstValue) -> None:
        # TODO sprawdzenie czy nie jest to string

        self.variables.append(int(element.value))


    def visit_block(self, element: Block) -> None:
        tmp_context = self.current_context
        self.current_context = Context(parent_context=self.current_context)

        for s in element.statements:
            if self.return_flag:
                self.current_context = tmp_context
                return
            s.accept(self)



        self.current_context = tmp_context


    def visit_function_definition(self, element: FunctionDefinition) -> None:
        self.current_context.define_definition(element.name, element)

        if len(element.on_condition) > 0:
            fun_on = FunctionOn(element, self, self.current_context)
            self.is_collecting_contexts = True
            element.on_condition[0].accept(self)
            for c in self.contexts_to_observe:
                c.attach(fun_on)
            self.is_collecting_contexts = False
            self.contexts_to_observe = []

        elif len(element.each_expression) > 0:
            FunctionEach(element, self, self.current_context)



    def visit_function_call(self, element: FunctionCall) -> None:

        if element.name == "return":
            if len(element.arguments) > 1:
                raise RuntimeError("Wrong number of returned elements. Can return only one element")
            element.arguments[0].accept(self)
            self.return_flag = True
            return


        context = self.current_context.check_element_defined(element.name)



        if isinstance(context.definitions.get(element.name), SpecialFunctions):
            function_execution = context.definitions.get(element.name)
            function_execution.execute(self, self.current_context, element)
            return
        elif isinstance(context.elements.get(element.name), SpecialFunctions):
            function_execution = context.elements.get(element.name)
            function_execution.execute(self, self.current_context, element)
            return
        


        function_execution : FunctionDefinition
        if isinstance(context.definitions.get(element.name), FunctionDefinition):
            function_execution = context.definitions.get(element.name)
        elif isinstance(context.elements.get(element.name), FunctionDefinition):
            function_execution = context.elements.get(element.name)
        else:
            raise RuntimeError("Called object is not a function")


        if len(function_execution.arguments) != len(element.arguments):
            raise RuntimeError("Wrong number of arguments")

        tmp_context = self.current_context
        

        self.current_context = Context(parent_context=self.current_context)
        for arg in range(0,len(element.arguments)):
            element.arguments[arg].accept(self)
            if self.id_value == "return":
                raise RuntimeError("Invalid use of return")
            val = self.variables.pop()
            self.current_context.elements[function_execution.arguments[arg]] = val;

        function_execution.block.accept(self)

        if self.return_flag is not True:
            self.variables.append(0)

        self.return_flag = False

        self.current_context = tmp_context


    def visit_function_call_or_value(self, element: FunctionCallOrValue) -> None:
        is_in_object = False
        get_r_value = self.get_r_value
        do_checking = self.do_checking
        tmp_context = self.current_context

        for el in range (0,len(element.operands)-1):
            is_in_object = True
            self.do_checking = True
            self.get_r_value = False
            if element.operands[el].name == 'return':
                raise RuntimeError("Invalid use of return")
            element.operands[el].accept(self)
            val = self.current_context.get_possible_object(self.id_value)
            if val is None:
                raise RuntimeError("Object doesn't exist")
            else:
                while isinstance(val, str):
                    val = self.current_context.get_possible_object(val)
                if isinstance(val, ObjectContext):
                    self.current_context = val
                else:
                    raise RuntimeError("Called element is not an object")

        self.get_r_value = get_r_value
        x = element.operands[-1].name
        if is_in_object:
            self.do_checking = False
            if self.current_context.elements.get(element.operands[-1].name ) is None:
                if self.current_context.definitions.get(element.operands[-1].name) is None:
                    raise RuntimeError("Element doesn't exist")
            element.operands[-1].accept(self)
        else:
            self.do_checking = do_checking
            element.operands[-1].accept(self)

        if not self.is_assign:
            self.current_context = tmp_context 



    def visit_if_statement(self, element: IfStatement) -> None:
        
        element.condition.accept(self)

        value = self.variables.pop()

        if value is True:
            element.true_block.accept(self)
        else:
            if element.false_block:
                element.false_block.accept(self)


    def visit_while_statement(self, element: WhileStatement) -> None:

        element.condition.accept(self)

        value = self.variables.pop()

        while value:
            element.block.accept(self)
            element.condition.accept(self)
            value = self.variables.pop()


    def visit_foreach_statement(self, element: ForeachStatement) -> None:

        param1 = element.param[0]
        param2 = element.param[1]

        elem = []
        for obj in self.global_context.objects.values():
            for key, val in obj.elements.items():
                if key == param1:
                    elem.append(val)
        
        for val in elem:
            temp_context = self.current_context
            self.current_context = Context(parent_context=self.current_context)

            self.current_context.elements[param2] = val

            element.block.accept(self)

            self.current_context = temp_context
            
        


    def visit_value_assignment(self, element: ValueAssignment) -> None:
        tmp_current_context = self.current_context
        self.do_checking = False
        self.is_assign = True
        self.get_r_value = False
        element.name.accept(self)
        name = self.id_value

        assign_context = self.current_context
        self.current_context = tmp_current_context

        self.do_checking = True
        self.is_assign = False
        self.get_r_value = True
        element.expression.accept(self)
        value = self.variables.pop()
        assign_context.define_element(name, value)

    def check_string(self, op1, op2):
        if isinstance(op1, str) or isinstance(op2, str):
            raise RuntimeError("Invalid operation on string")


class FunctionOn(Observer):
    def __init__(self, function_definition : FunctionDefinition, visitor: InterpreterVisitor, context : Context):
        self.visitor = visitor
        self.function_definition = function_definition
        self.context = context
        self.condition_val = None
        self.function_call = FunctionCall(self.function_definition.location, 
                                        self.function_definition.name, [])

        if len(self.function_definition.arguments):
            raise RuntimeError("Function with on statement can no receive arguments")

        self.execute()

    def update(self, subject: Subject) -> None:
        self.execute()

    def execute(self):
        self.function_definition.on_condition[0].accept(self.visitor)
        val = self.visitor.variables.pop()

        if val and self.condition_val != val:
            self.condition_val = val
            temp = self.visitor.current_context
            self.visitor.current_context = self.context
            self.visitor.visit_function_call(self.function_call)
            self.visitor.current_context = temp
            print("dziala")
        else:
            self.condition_val = val

class FunctionEach():
    def __init__(self, function_definition : FunctionDefinition, visitor: InterpreterVisitor, context : Context):
        self.visitor = visitor
        self.function_definition = function_definition
        self.context = context
        self.function_call = FunctionCall(self.function_definition.location, 
                                        self.function_definition.name, [])
        self.start()

    def execute(self):
        temp = self.visitor.current_context
        self.visitor.current_context = self.context
        self.visitor.visit_function_call(self.function_call)
        self.visitor.current_context = temp


    def start(self):
        self.function_definition.each_expression[0].accept(self.visitor)
        timeout = self.visitor.variables.pop()

        if self.function_definition.time_describer == 'ms':
            timeout = timeout / 1000
        elif self.function_definition.time_describer == 's':
            timeout = timeout 
        elif self.function_definition.time_describer == 'min':
            timeout = timeout * 60
        elif self.function_definition.time_describer == 'h':
            timeout = timeout * 60 * 60
        elif self.function_definition.time_describer == 'd':
            timeout = timeout * 60 * 60 * 24

        l = task.LoopingCall(self.execute)
        l.start(timeout)
        reactor.run()
 
 