#from .Token import Location

class QuoteError(BaseException):
    pass
class UnexpectedCharacterError(BaseException):
    pass

class ParserError(BaseException):
    pass

class RuntimeError(BaseException):
    pass